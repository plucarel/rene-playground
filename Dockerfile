FROM centos/python-27-centos7

EXPOSE 8080

# Install yum packages with the ROOT user
USER root
RUN yum install -y epel-release
RUN yum install -y python-devel python-virtualenv

# Make the application folder with its virtual environment folder
# COPY . /opt/app-root
COPY ./etc /opt/app-root/etc
RUN chmod ug+x /opt/app-root/etc/entrypoint.sh

RUN mkdir  -p /opt/app-root/venv

# Set the permissions for the app-user user
RUN chown 1001:0 /opt/app-root && chmod -R ug+rwx /opt/app-root
# RUN chmod ug+x /opt/app-root/entrypoint.sh

USER 1001
WORKDIR /opt/app-root
# Setup the virtual environment
RUN virtualenv /opt/app-root/venv/

# Running pip
ARG pip="/opt/app-root/venv/bin/pip"
RUN ${pip} install --upgrade pip
# Install uwsgi Python web server
RUN ${pip} install uwsgi
# Install pip requirements
COPY ./requirements.txt /opt/app-root/requirements.txt
RUN ${pip} install -r requirements.txt 
# Copy the application files
COPY ./app.py /opt/app-root

ENTRYPOINT /opt/app-root/etc/entrypoint.sh